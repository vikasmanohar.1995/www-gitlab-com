---
layout: handbook-page-toc
title: "Team Member Relations Philosophy"
description: "Information on GitLab's Team Member Relations Philosophy."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Team Member Relations Philosophy

GitLab believes in preserving the dignity of each team member and expects everyone to treat others with fairness, respect, and transparency.  We encourage mutual responsibility for constructive work relationships and communication, information sharing, problem solving and a safe neutral processs through which differences can be resolved.  Direct and honest communication is strongly encouraged between all team members regardless of title or level and per our values we [do not pull rank](https://about.gitlab.com/handbook/values/#dont-pull-rank). Such communication is likely to enhance understanding, avoid misunderstandings and create rapid solutions to concerns.  

We have built this philosophy to focus on these 4 pillars in line with our values:

- [Collaboration](https://about.gitlab.com/handbook/values/#collaboration)
- [Diversity, Inclusion & Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion)
- [Results](https://about.gitlab.com/handbook/values/#results)
- [Transparency](https://about.gitlab.com/handbook/values/#transparency)

## Commitments

- We are committed to providing a safe environment for all GitLab team members to achieve maximum career development and goal achievement.
- We are committed to treating each team member as an individual and providing a workplace where communication is transparent and where problems can be discussed and resolved in a mutually respectful environment. To foster this environment we take into account individual team member circumstances and the individual team member while balancing the needs of the business.  

## Team Member Relations Function

To support our team members, GitLab has established a [Team member relations group](https://about.gitlab.com/handbook/people-group/#team-member-relations-specialist) to assist team members and their leaders with resolving work related issues.  The Team Member Relations Specialist (TMR) respects the privacy of all team members and treats discussions with the fullest degree of confidentiality possible.  We have incorporated our [Values](https://about.gitlab.com/handbook/values/) and the [Code of Conduct](https://about.gitlab.com/handbook/legal/gitlab-code-of-business-conduct-and-ethics/) into our team member relations practices and philosophy.  We believe that by communicating with each other directly and transparently, we can continue to resolve any difficulties that may arise and continue to make GitLab a great place to work.  

### For Team Members

The team member relations function provides all GitLab team members an avenue to express workplace concerns and to resolve conflicts in a safe and unbiased forum.  

- Team members can express themselves openly and freely without fear of retaliation.
- Professional behavior and conduct is expected from all team members.  As a reminder use judgement in your conversations with other team members.  We encourage all team members to [provide direct feedback](https://about.gitlab.com/handbook/leadership/#sts=Giving%20Feedback) to each other. The team member relations group is here to listen to team members concerns in an unbiased, open and professional manner. 

### For Managers

Team member relations provides guidance to managers in their efforts to improve team member performance or to correct unacceptable personal behaviors.  Managers are responsible for setting priorities and motivating their team members.  They are also responsible for ensuring the care of their team members as well as meeting GitLab's goals.  These two things can be done simultaneously.  As managers it is considered one of your primary responsibilities to understand GitLab's [Code of Conduct](https://about.gitlab.com/handbook/legal/gitlab-code-of-business-conduct-and-ethics/), [Values](https://about.gitlab.com/handbook/values/) and [People Group](https://about.gitlab.com/handbook/people-group/) policies and processes.  People managers are responsible for upholding compliance on the their teams and considering the best interest of the business.  If a manager is made aware of a situation that potentially runs afoul of the Code of Conduct, Values and People Group processes or policy they should immediately reach out to the [team member relations specialist](https://about.gitlab.com/handbook/people-group/#team-member-relations-specialist).  If uncertain about a specific policy or procedure, the manager should reach out to a leader, their [aligned PBP](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division) or the Team member relation specialist for further clarification.  When in doubt, it is always best to ask.  

## Team Member Performance Improvement Management Guiding Principles

A critical responsibility within the role of a GitLab leader is to spend the time required to truly understand the causes of the performance issue and how to address them properly.  GitLab managers will determine if the performance concerns are skill-based or behavior (will)-based to determine next steps.  Below are the definitions of both skill- and will-based performance concerns. 

### Skill-based performance management

- Team member lacks the technical knowledge or capability to be successful in the role
- Team member is unable to prioritize work effectively
- Team member lacks the understanding on how to complete tasks
- Team member exhibits difficulties working with team members and/or communicating effectively

Sometimes skill-based issues appear as a will-based or behavior concern.  However, after delving into the cause for the behavior, the leader may find a skill-based gap causing the disruptive behavior.  The team member may be embarrassed or concerned about their ineffectiveness or ability to perform their role and may react in a way that is inappropriate.

The following are a few recommendations for a manager to address skill-based performance issues:

- Provide the team member with additional training
- Provide the team member with a mentor or buddy
- Provide the team member with clear directions and examples

If the team member is still not making progress after the manager has provided additional resources or coaching please review the [underperformance page in the handbook](https://about.gitlab.com/handbook/leadership/underperformance/) for next steps. 

### Will-based performance management
Will-based issues are described as undesirable behavior that impedes the success of the Team Member, the Team, and/or GitLab as a whole.  The leader may hear about the behaviors from others or experience the undesirable behavior directly.   It is important that the manager address the concerns right away.  While "will" issues can be disruptive, there are different levels of severity which must be considered when determining next steps.  The tiers below are examples and are not exhaustive, and should be used for comparision purposes to determine the impact to GitLab.

**Tier 2** - Misconduct resulting in limited material risk to GitLab

- Violations of the company code of conduct (excluding harassment and discrimination)
- Team member uses inappropriate language that others may find offensive
- Team members behavior creates a negative or toxic team environment
- Behavior continues even after coaching from manager

For Tier 2 level concerns, the Manager should reach out and discuss with the team member relations specialist immediately.  

**Tier 1** - Gross misconduct or a violation with serious implications to GitLab.    

- Violates a law/regulation (theft, fraud, drug use, etc.)
- Poses a serious operational, reputational, or financial risk to GitLab
- Poses significant health or safety risks
- Harassment
- Severe and/or pervasive language or conduct that could be perceived as discriminatory or creating a hostile work environment

Since situations differ, managers should immediately reach out to a team member relations specialist for guidance before taking any action.  If unable to contact the team member relations specialist directly, please follow the [escalation path listed in the handbook](https://about.gitlab.com/handbook/people-group/)

Please review the [underperformance](https://about.gitlab.com/handbook/leadership/underperformance/) page in the handbook for further information regarding managing team member performance.  










