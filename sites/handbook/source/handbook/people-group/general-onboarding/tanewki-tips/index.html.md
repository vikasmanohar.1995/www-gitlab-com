---
layout: handbook-page-toc
title: "TaNewKi Tips"
description: "A Guide for new team members."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc}

## TaNewKi Tips

## <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> Welcome!
{: #tanuki-orange}

Hello! We could not be more excited to have you here. This page is here to help walk you through what you can expect before and during onboarding as a new team member. 


## <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> Meet the People Experience Team! 
{: #tanuki-orange}

[The People Experience Team] (https://about.gitlab.com/handbook/people-group/people-experience-team/) is here to help guide you in your journey here at GitLab. One of us will be assigned to your Onboarding Issue, and will be there to help guide you as you begin onboarding. 

## <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> Timeline 
{: #tanuki-orange}
After you sign your offer and complete your background check and references, you can expect your onboarding to go like this: 

##### Before you start

1. Sign your offer, and be on the lookout for your Welcome Email from the Candidate Experience Specialist team. You can read about the team on [this handbook page](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/coordinator/).
    - this email will contain all pertainent information such as how to order your office equipment and most importantly your laptop. The IT team also has a very handy, handbook page [you can review](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/#laptops).
1. If you are being employed by one of GitLab's US entities, either GitLab, Inc or GitLab, Fed, you will need to [complete your I9 via LawLogix](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#timing-of-i-9). You will receive an email from LawLogix. This email will walk you through the process of completing the I9. 
1. If you live in Germany, South Korea, Japan, France, or the UK you will need to be on a lookout from an email from the People Experience Team (via DocuSign or directly from the team) to complete some payroll documents. 
1. The next email you will receive, is a [TaNEWki call](https://about.gitlab.com/handbook/people-group/general-onboarding/#tanuki-orange) invite. We hope you can join us! This Zoom call addresses those first day nerves and gives you some time to meet other new team members. You will see this invite sent to you 1-2 weeks before your start date. 

##### First day :tada: 

1. 
